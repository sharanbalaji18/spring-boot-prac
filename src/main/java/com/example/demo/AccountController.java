package com.example.demo.src.main.java.com.example.demo;



import com.example.demo.Account;
import com.example.demo.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountController{
    @Autowired
    private AccountService accountService;
    @RequestMapping("/account")
    public List<Account> get(){
        return accountService.get();
    }

    @PostMapping(value="/accountCreation")
    public void addAccount(@RequestBody Account account){
        accountService.addAccount(account);
    }

    @PutMapping(value="/accountUpdate/{firstName}")
    public void updateAccount(@PathVariable String firstName,@RequestBody Account account){
        accountService.updateAccount(firstName,account);
    }

    @DeleteMapping(value="/accountDelete/{firstName}")
    public void deleteAccount(@RequestBody Account account,@PathVariable String firstName){
        accountService.deleteAccount(firstName);
    }

}

