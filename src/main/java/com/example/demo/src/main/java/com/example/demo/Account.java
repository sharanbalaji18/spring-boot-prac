package com.example.demo.src.main.java.com.example.demo.src.main.java.com.example.demo;



import java.util.zip.ZipEntry;

public class Account {
    private String password;
    private String AccountHolderName;
    private String firstName;

    public Account(String AccountHolderName,String password,String firstName) {
        this.AccountHolderName=AccountHolderName;
        this.password=password;
        this.firstName=firstName;

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        password = password;
    }

    public String getAccountHolderName() {
        return AccountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        AccountHolderName = accountHolderName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}

