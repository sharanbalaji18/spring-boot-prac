package com.example.demo.src.main.java.com.example.demo;


import com.example.demo.Account;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AccountService{

    private List<Account> acc=new ArrayList<>(Arrays.asList(
            new Account("api","Apishek@13","axjwnbd"),
            new Account("sri","sriram@003","apiju")
    ));

    public void addAccount(Account account) {
        acc.add(account);
    }

    public List<Account> get() {
        return acc;
    }

    public void updateAccount(String firstName, Account account) {
        for(int i=0;i<acc.size();i++){
            Account a=acc.get(i);
            if(a.getFirstName().equals(firstName)){
                acc.set(i,account);
                return;
            }
        }
    }

    public void deleteAccount(String firstName) {
        for(int i=0;i<acc.size();i++){
            Account a= acc.get(i);
            if(a.getFirstName().equals(firstName)){
                acc.remove(i);
                return;
            }
        }
    }
}

