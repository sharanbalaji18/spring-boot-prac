package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class tcontrol {
    @Autowired
    private tservice Tservice;
    @RequestMapping("/topicss")
    public List<Topic> getAllTopics(){
        return Tservice.getAllTopics();
    }
    @RequestMapping("/topicss/{id}")
    public Topic getTopic(@PathVariable String id){
        return Tservice.getTopic(id);
    }
    @RequestMapping(method= RequestMethod.POST, value="/topicss")
    public void addTopic(@RequestBody Topic topic){
        Tservice.addTopic(topic);
    }
}
