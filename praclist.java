package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class praclist {
    @RequestMapping("/topics")
    public List<Topic> getAllTopics(){
        return Arrays.asList(
          new Topic("s","sf","sfd"),new Topic("j","jf","jfd"),new Topic("js","jsf","jsfd")
        );
    }
}
