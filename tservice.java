package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class tservice {
    private List<Topic> topicss=new ArrayList<>(Arrays.asList(new Topic("s","sf","sfd"),new Topic("j","jf","jfd"),new Topic("js","jsf","jsfd")
    ));
    public List<Topic> getAllTopics(){
        return topicss;
    }
    public Topic getTopic(String id){
        return topicss.stream().filter(t->t.getId().equals(id)).findFirst().get();
    }
    public void addTopic(Topic topic){
        topicss.add(topic);
    }
}
